
create table tb_article(
  id          int primary key  auto_increment,
  title       varchar (200),
  description varchar (200),
  category_id int,
  author      varchar (200),
  thumbnail   varchar (200),
  create_date varchar (200));

create table tb_categories(
  category_id          int primary key auto_increment,
  name        varchar (200) not null
);

