package com.example.demo.controller;

import com.example.demo.model.Article;
import com.example.demo.model.Category;
import com.example.demo.service.ArticleService;
import com.example.demo.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
public class HomeController {

    private ArticleService articleService ;
    private CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Autowired
    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }

    String s_title;
    Integer s_id;
    boolean isSearch;
    @GetMapping("/")
    public String index(Model m, @RequestParam(value = "catId",required = false)Integer catId,@RequestParam(value = "title",required = false)String title){
        System.out.println(title);
            if(title==null) {
                isSearch = false;
                m.addAttribute("Article", new Article());
                List<Article> articles = articleService.showPage(0, 10);
                m.addAttribute("pages", articleService.page(articleService.findAll()));
                m.addAttribute("articles", articles);

            }else {
                System.out.println(title);
                isSearch = true;
                s_title = title;
                s_id = catId;
                m.addAttribute("isSearch",isSearch);
                if (catId==0){
                    List<Article> pages = articleService.search(title);
                    m.addAttribute("pages", articleService.page(pages));
                    List<Article> articles = articleService.searchPage(title,0,10);
                    m.addAttribute("articles", articles);
                }else {
                    List<Article> pages = articleService.searchArtByCat(title,catId);
                    m.addAttribute("pages", articleService.page(pages));
                    List<Article> articles = articleService.searchPageArtByCat(title,catId,0,10);
                    m.addAttribute("articles", articles);
                }
            }

            m.addAttribute("curPage", 1);
            List<Category> categories = categoryService.findAll();
            m.addAttribute("categories", categories);

        return "index";
    }


    @GetMapping("/category")
    public  String category(Model m){

        List<Category> pages = categoryService.findAll();
        m.addAttribute("pages",categoryService.page(pages));
        List<Category> categories = categoryService.showPage(0,5);
        m.addAttribute("categories",categories);
        m.addAttribute("curPage",1);

        return "category";
    }

    @GetMapping("category/page/{page}")
    public String showPageCat(Model m,@PathVariable Integer page){
        List<Category> categories = categoryService.showPage( (page*5)-5,  5);
        m.addAttribute("categories", categories);
        m.addAttribute("pages", categoryService.page(categoryService.findAll()));
        System.out.println(categoryService.page(categoryService.findAll()));
        m.addAttribute("nextPage", page + 1);
        m.addAttribute("PreviousPage", page - 1);
        m.addAttribute("curPage", page);
        return "category";
    }

    @GetMapping("/page/{page}")
    public String showPage(Model m,@PathVariable Integer page){
        if(!isSearch) {
            m.addAttribute("isSearch",isSearch);
            List<Article> articles = articleService.showPage( (page - 1) * 10,  10);
            m.addAttribute("articles", articles);
            m.addAttribute("pages", articleService.page(articleService.findAll()));
            m.addAttribute("nextPage", page + 1);
            m.addAttribute("PreviousPage", page - 1);
            m.addAttribute("curPage", page);
            List<Category> categories = categoryService.findAll();
            m.addAttribute("categories", categories);
        }else {
            if (s_id==0){
                List<Article> pages = articleService.search(s_title);
                m.addAttribute("pages", articleService.page(pages));
                List<Article> articles = articleService.searchPage(s_title,(page-1)*10,10);
                m.addAttribute("articles", articles);

            }else {
                List<Article> pages = articleService.searchArtByCat(s_title,s_id);
                m.addAttribute("pages", articleService.page(pages));
                List<Article> articles = articleService.searchPageArtByCat(s_title,s_id,(page-1)*10,10);
                m.addAttribute("articles", articles);

            }
            m.addAttribute("curPage", page);
            m.addAttribute("nextPage", page + 1);
            m.addAttribute("PreviousPage", page - 1);
            List<Category> categories = categoryService.findAll();
            m.addAttribute("categories", categories);
        }

        return "index";
    }

    @GetMapping("/add")
    public String add(ModelMap m){
        m.addAttribute("article",new Article());
        m.addAttribute("isAdd", true);
        List<Category> categories = categoryService.findAll();
        m.addAttribute("categories",categories);
        System.out.println("GetMapping : Add");

        return "add";
    }

    @PostMapping("/add")
    public String insert(@Valid @ModelAttribute Article article, BindingResult result,Model m,@RequestParam("file") MultipartFile file){
        System.out.println("PostMapping : Add");
        if(result.hasErrors()){
            m.addAttribute("article",article);
            m.addAttribute("isAdd", true);
            List<Category> categories = categoryService.findAll();
            m.addAttribute("categories",categories);

            return "add";
        }else {
            String filename = UUID.randomUUID().toString() + file.getOriginalFilename();
            if(!file.isEmpty()){
                System.out.println(file.getOriginalFilename());
                article.setImageName(filename);
                try {
                    Files.copy(file.getInputStream(), Paths.get(System.getProperty("user.dir")+"\\src\\main\\resources\\images\\", filename));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else {
                article.setImageName("placeholder-image.png");
            }
                System.out.println(article.getCategory().getId());
                articleService.add(article);
                return "redirect:/";
        }
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Integer id){
        articleService.delete(id);
        return "redirect:/";
    }
    @GetMapping("/update/{id}")
    public String update(Model m, @PathVariable Integer id){
        m.addAttribute("article", articleService.findById(id));
        m.addAttribute("isAdd", false);
        List<Category> categories = categoryService.findAll();
        m.addAttribute("categories",categories);
        System.out.println("GetMapping : Update");
        return "add";
    }
    @PostMapping("/update")
    public String saveUpdate(@Valid @ModelAttribute Article article,BindingResult result,Model m,@RequestParam("file") MultipartFile file){
        System.out.println("PostMapping : Update");
        if(result.hasErrors()){
            List<Category> categories = categoryService.findAll();
            m.addAttribute("categories",categories);
            m.addAttribute("article",article);
            m.addAttribute("isAdd", false);
            return "add";
        }else {
            article.setCreatedDate(new Date().toString());
            String filename = UUID.randomUUID().toString() + file.getOriginalFilename();
            if (!file.isEmpty()) {
                System.out.println(file.getOriginalFilename());
                article.setImageName(filename);
                try {
                    Files.copy(file.getInputStream(), Paths.get(System.getProperty("user.dir") + "\\src\\main\\resources\\images\\", filename));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else {
                article.setImageName(article.getImageName());
            }
            article.setCreatedDate(new Date().toString());
            articleService.update(article);
            return "redirect:/";
        }
    }
    @GetMapping("/view/{id}")
    public String viewItem(Model m,@PathVariable Integer id){
        Article article = articleService.findById(id);



        m.addAttribute("article",article);

        return "view";
    }

    @GetMapping("/addCategory")
    public String addCategory(ModelMap m){
        m.addAttribute("category",new Category());
        m.addAttribute("isAddCat", true);
        System.out.println("GetMapping : Add");

        return "addCategory";
    }

    @PostMapping("/addCategory")
    public String insertCat(@Valid @ModelAttribute Category category, BindingResult result,Model m){
        if(result.hasErrors()){
            m.addAttribute("category",category);
            m.addAttribute("isAddCat", true);

            return "addCategory";
        }
            categoryService.add(category);
            return "redirect:/category";
    }
    @GetMapping("/deleteCategory/{id}")
    public String deleteCate(@PathVariable Integer id){
        categoryService.delete(id);
        return "redirect:/category";
    }
    @GetMapping("/updateCategory/{id}")
    public String updateCat(Model m, @PathVariable Integer id){
        m.addAttribute("category", categoryService.findById(id));
        m.addAttribute("isAddCat", false);
        return "addCategory";
    }

    @PostMapping("/updateCategory")
    public String saveUpdateCat(@Valid @ModelAttribute Category category,BindingResult result,Model m){
        if(result.hasErrors()){
            m.addAttribute("category",category);
            m.addAttribute("isAddCat", false);
            return "addCategory";
        }
            categoryService.update(category);
            return "redirect:/category";
        }




}
