package com.example.demo.repository.provider;

import com.example.demo.model.Article;
import org.apache.ibatis.jdbc.SQL;

public class ArticleProvider{

    public  String findAll(){
        return "Select a.id,a.thumbnail,a.title, a.description, a.author," +
                "a.create_date,a.category_id,c.name from tb_article a INNER JOIN tb_categories c " +
                "ON a.category_id = c.category_id";
    }

    public  String showPage(int offset,int limit){
        return "Select a.id,a.thumbnail,a.title, a.description, a.author," +
                "a.create_date,a.category_id,c.name from tb_article a INNER JOIN tb_categories c " +
                "ON a.category_id = c.category_id limit "+limit+" offset "+ offset ;
    }

    public String findId(Integer id) {

            return "Select a.id,a.thumbnail,a.title, a.description, a.author," +
                    "a.create_date,a.category_id,c.name from tb_article a INNER JOIN tb_categories c " +
                    "ON a.category_id = c.category_id " +
                    "where a.id = "+id;


    }
    public String delete(Integer id){
        return new SQL(){{
            DELETE_FROM("tb_article ");
            WHERE("id =" + id);
        }}.toString();
    }

    public String insert(Article article){
        return new SQL(){{
            INSERT_INTO("tb_article");
            VALUES("thumbnail","'"+article.getImageName()+"'");
            VALUES("title","'"+article.getTitle()+"'");
            VALUES("description","'"+article.getDescription()+"'");
            VALUES("author","'"+article.getAuthor()+"'");
            VALUES("create_date","'"+article.getCreatedDate()+"'");
            VALUES("category_id","'"+article.getCategory().getId()+"'");

        }}.toString();
    }
    public String updateItem(Article article){
        return new SQL(){{
            UPDATE("tb_article");
            SET("thumbnail =" + "'"+article.getImageName()+"'",
                     "title = " + "'"+article.getTitle()+"'",
                     "description = " + "'"+article.getDescription()+"'",
                     "author = " + "'"+article.getAuthor()+"'",
                    "category_id =" + "'"+article.getCategory().getId()+"'");
            WHERE("id = " + article.getId());

        }}.toString();
    }
    public String searchPage(String title,int offset, int limit){
        return "Select a.id,a.thumbnail,a.title, a.description, a.author,a.create_date,a.category_id,c.name" +
                " from tb_article a INNER JOIN tb_categories c ON a.category_id = c.category_id where a.title ilike '%"+title+"%'"+"limit "+limit+" offset "+offset;
    }
    public String searchItem(String title){
        return "Select a.id,a.thumbnail,a.title, a.description, a.author,a.create_date,a.category_id,c.name" +
                " from tb_article a INNER JOIN tb_categories c ON a.category_id = c.category_id where a.title ilike '%"+title+"%'";
    }
    public String searchArtByCat(String title,Integer id){
        return "Select a.id,a.thumbnail,a.title, a.description, " +
                "a.author,a.create_date,a.category_id,c.name from tb_article a" +
                " INNER JOIN tb_categories c ON a.category_id = c.category_id where a.title ilike '%"+title+"%' AND  c.category_id = "+ id;
    }

    public String searchPageArtByCat(String title,Integer id,int offset,int limit){
        return "Select a.id,a.thumbnail,a.title, a.description, " +
                "a.author,a.create_date,a.category_id,c.name from tb_article a" +
                " INNER JOIN tb_categories c ON a.category_id = c.category_id where a.title ilike '%"+title+"%' AND  c.category_id = "+ id +" limit "+limit+" offset "+offset;
    }

}
