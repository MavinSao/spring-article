package com.example.demo.repository.provider;

import com.example.demo.model.Category;
import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {

    public String findAll(Integer id) {
        return new SQL(){{
            SELECT("category_id,name");
            if(id!=null){
                WHERE("category_id="+id);
            }
            FROM("tb_categories");


        }}.toString();
    }
    public String delete(Integer id){
        return new SQL(){{
            DELETE_FROM("tb_categories");
            WHERE("category_id =" + id);
        }}.toString();
    }

    public String insert(Category category){
        return new SQL(){{
            INSERT_INTO("tb_categories");
            VALUES("name","'"+category.getName()+"'");
        }}.toString();
    }
    public String updateItem(Category category){
        return new SQL(){{
            UPDATE("tb_categories");
            SET("name =" + "'"+category.getName()+"'");
            WHERE("category_id = " + category.getId());

        }}.toString();
    }

    public  String showPage(int offset,int limit){
        return "Select category_id,name from tb_categories limit "+ limit + " offset "+offset;
    }

}
