package com.example.demo.repository;

import com.example.demo.model.Article;
import com.example.demo.repository.provider.ArticleProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository {


    @SelectProvider(method = "findAll",type = ArticleProvider.class)
    @Results({
            @Result(property = "imageName",column = "thumbnail"),
            @Result(property = "createdDate",column = "create_date"),
            @Result(property = "category.id",column = "category_id"),
            @Result(property = "category.name",column = "name"),


    })
    List<Article> findAll();

    @SelectProvider(method = "showPage",type = ArticleProvider.class)
    @Results({
            @Result(property = "imageName",column = "thumbnail"),
            @Result(property = "createdDate",column = "create_date"),
            @Result(property = "category.id",column = "category_id"),
            @Result(property = "category.name",column = "name"),


    })
    List<Article> showPage(int offset,int limit);

   @SelectProvider(method = "findId",type = ArticleProvider.class)
   @Results({
           @Result(property = "imageName",column = "thumbnail"),
           @Result(property = "createdDate",column = "create_date"),
           @Result(property = "category.id",column = "category_id"),
           @Result(property = "category.name",column = "name"),


   })
    Article findById(int id);
    @SelectProvider(method = "insert",type = ArticleProvider.class)
    void add(Article article);

    @SelectProvider(method = "delete",type = ArticleProvider.class)
    void delete(int id);

    @SelectProvider(method = "updateItem",type = ArticleProvider.class)
    void update(Article article);


    @SelectProvider(method = "searchItem",type = ArticleProvider.class)
    @Results({
            @Result(property = "imageName",column = "thumbnail"),
            @Result(property = "createdDate",column = "create_date"),
            @Result(property = "category.id",column = "category_id"),
            @Result(property = "category.name",column = "name"),
    })
    List<Article> search(String title);

    @SelectProvider(method = "searchPage",type = ArticleProvider.class)
    @Results({
            @Result(property = "imageName",column = "thumbnail"),
            @Result(property = "createdDate",column = "create_date"),
            @Result(property = "category.id",column = "category_id"),
            @Result(property = "category.name",column = "name"),
    })
    List<Article> searchPage(String title,int offset,int limit);

    @SelectProvider(method = "searchArtByCat",type = ArticleProvider.class)
    @Results({
            @Result(property = "imageName",column = "thumbnail"),
            @Result(property = "createdDate",column = "create_date"),
            @Result(property = "category.id",column = "category_id"),
            @Result(property = "category.name",column = "name"),
    })
    List<Article> searchArtByCat(String title,Integer id);

    @SelectProvider(method = "searchPageArtByCat",type = ArticleProvider.class)
    @Results({
            @Result(property = "imageName",column = "thumbnail"),
            @Result(property = "createdDate",column = "create_date"),
            @Result(property = "category.id",column = "category_id"),
            @Result(property = "category.name",column = "name"),
    })
    List<Article> searchPageArtByCat (String title,Integer id,int offset,int limit);






}
