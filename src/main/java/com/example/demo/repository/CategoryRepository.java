package com.example.demo.repository;

import com.example.demo.model.Article;
import com.example.demo.model.Category;
import com.example.demo.repository.provider.ArticleProvider;
import com.example.demo.repository.provider.CategoryProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {

    @SelectProvider(method = "findAll", type = CategoryProvider.class)
    @Results({
            @Result(property = "id", column = "category_id")
    })
    List<Category> findAll();

    @SelectProvider(method = "findAll", type = CategoryProvider.class)
    @Results({
            @Result(property = "id", column = "category_id")
    })
    Category findById(int id);
    @SelectProvider(method = "insert",type = CategoryProvider.class)
    void add(Category category);


    @SelectProvider(method = "delete",type = CategoryProvider.class)
    void delete(int id);


    @SelectProvider(method = "updateItem",type = CategoryProvider.class)
    void update(Category category);

    @SelectProvider(method = "showPage",type = CategoryProvider.class)
    @Results({
            @Result(property = "id", column = "category_id")
    })
    List<Category> showPage(int offset, int limit);
}
