package com.example.demo.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class Article {

   
    private Integer id;

    @NotBlank(message = "* Field cannot be blank")
    private String title;

    @NotBlank(message = "* Field cannot be blank")
    private String description;

    @NotBlank(message = "* Field cannot be blank")
    @Size(min = 3, max = 20,message = "* length must be > 3 and <20")
    private String author;

    private String imageName;

    private Category category;

    private String createdDate;



    public Article(){

    }

    public Article(Integer id, String title, String description, String author,String imageName, String createdDate,Category category) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.author = author;
        this.imageName = imageName;
        this.createdDate = createdDate;
        this.category =category;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", author='" + author + '\'' +
                ", imageName='" + imageName + '\'' +
                ", category=" + category +
                ", createdDate='" + createdDate + '\'' +
                '}';
    }
}
