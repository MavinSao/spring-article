package com.example.demo.service;

import com.example.demo.model.Article;
import com.example.demo.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ArticleServiceImpl implements ArticleService {

    private ArticleRepository articleRepository;

    @Autowired
    public void setArticleRepository(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }
    @Override
    public List<Article> findAll() {
        return articleRepository.findAll();
    }

    @Override
    public Article findById(int id)
    {
        return articleRepository.findById(id);
    }

    public void add(Article article){
        article.setCreatedDate(new Date().toString());
        articleRepository.add(article);
    }
    public void delete(int id){
        articleRepository.delete(id);
    }
    public  void update(Article article){
        article.setCreatedDate(new Date().toString());
        articleRepository.update(article) ;
    }

    @Override
    public List<Article> search(String title) {
        return articleRepository.search(title);
    }

    public  List<Article> showPage(int first,int last){
        return articleRepository.showPage(first,last);
    }

    public List<Integer> page(List<Article> articles){
        List<Integer> pages = new ArrayList<>();
        int pageNum=0;
        for (int i=1; i<= articles.size() ; i++ ){

            if(i%10==0){
                pageNum = i/10;
                pages.add(pageNum);
            }
            if(i==articles.size()){
                if(i%10 != 0){
                    pages.add(pageNum+1);
                }
            }
        }
        return pages;
    }

    @Override
    public List<Article> searchArtByCat(String title, Integer id) {
        return articleRepository.searchArtByCat(title,id);
    }

    @Override
    public List<Article> searchPageArtByCat(String title, Integer id, int offset, int limit) {
        return articleRepository.searchPageArtByCat(title,id,offset,limit);
    }

    @Override
    public List<Article> searchPage(String title, int offset, int limit) {
        return articleRepository.searchPage(title,offset,limit);
    }


}
