package com.example.demo.service;

import com.example.demo.model.Category;

import java.util.List;

public interface CategoryService {
    List<Category> findAll();
    Category findById(int id);
    void add(Category category);
    void delete(int id);
    void update(Category category);
    List<Integer> page(List<Category> articles);
    List<Category> showPage(int offset, int limit);
}
