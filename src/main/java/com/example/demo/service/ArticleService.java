package com.example.demo.service;

import com.example.demo.model.Article;

import java.util.List;

public interface ArticleService {
    List<Article> findAll();
    Article findById(int id);
    void add(Article article);
    void delete(int id);
    void update(Article article);
    List<Article> search(String title);
    List<Article> showPage(int offset,int limit);
    List<Integer> page(List<Article> articles);
    List<Article> searchArtByCat(String title,Integer id);
    List<Article> searchPageArtByCat (String title,Integer id,int offset,int limit);
    List<Article> searchPage(String title,int offset,int limit);


}
