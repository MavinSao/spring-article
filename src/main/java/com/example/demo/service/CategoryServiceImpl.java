package com.example.demo.service;

import com.example.demo.model.Article;
import com.example.demo.model.Category;
import com.example.demo.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServiceImpl implements  CategoryService{

    private CategoryRepository categoryRepository;

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> findAll() {

        return categoryRepository.findAll();
    }

    @Override
    public Category findById(int id) {
        return categoryRepository.findById(id);
    }

    @Override
    public void add(Category category) {
        categoryRepository.add(category);
    }

    @Override
    public void delete(int id) {
        categoryRepository.delete(id);
    }

    @Override
    public void update(Category category) {
        categoryRepository.update(category);
    }

    public List<Integer> page(List<Category> categories){
        List<Integer> pages = new ArrayList<>();
        int pageNum=0;
        for (int i=1; i<= categories.size() ; i++ ){

            if(i%5==0){
                pageNum = i/5;
                pages.add(pageNum);
            }
            if(i==categories.size()){
                if(i%5 != 0){
                    pages.add(pageNum+1);
                }
            }
        }
        return pages;
    }

    @Override
    public List<Category> showPage(int offset, int limit) {
        return categoryRepository.showPage(offset,limit);
    }
}
